import requests, json

from card import Card
from cards import Cards
import json_utils

## load references, card libraries from set bundles
with open('set1-lite-en_us\\en_us\\data\\set1-en_us.json', encoding="utf8") as f:
    set1 = json.load(f)
with open('set2-lite-en_us\\en_us\\data\\set2-en_us.json', encoding="utf8") as f:
    set2 = json.load(f)

## Get info from the 21337 port
#---------------- Can we get smth from the localhost url ? ----------------------#
try:
    r = requests.get('http://127.0.0.1:21337')
except (requests.exceptions.ConnectionError, requests.exceptions.Timeout):
    print("Port not connected, please launch LoR and/or enable third party tools endpoints")
    exit()
else:
    print("Sucessfully connected")

#---------------- Handling and loading Decklist ---------------------------------#

# Check if not in game
static_decklist = json.loads(requests.get("http://127.0.0.1:21337/static-decklist").text)
#print("Decklist Info : ", static_decklist)

# Or for empty decklist

## Saving decklist for working on it without LoR opened
#with open('decklist.json', 'w') as outfile:
#    json.dump(static_decklist, outfile)

""" ## Loading the decklist as saved previously
with open('decklist.json', encoding="utf8") as f:
    static_decklist = json.load(f)"""

#print(json.dumps(static_decklist, indent=4))

# Load each card info from their set into Deck
deck = Cards(static_decklist["CardsInDeck"], "Deck", [set1, set2])
print(deck)


#---------------- Separating each card to their correct cards group -------------#

""" try:
    response = requests.get("http://127.0.0.1:21337/positional-rectangles")
except ConnectionRefusedError:
    print("No json returned") """

# card_positions = json.loads(response.text)
#print("Card positions : ", card_positions)
## The first two cards are the player cards (non interactable)

# Separate visible cards according to their x y pos
# Hand = new Cards(card list that x,y are ?,? )
# myBoard = new Cards(card list that x,y are ?,? )
# myBattle = new Cards(card list that x,y are ?,? )
# enBoard = new Cards(card list that x,y are ?,? )
# enBattle = new Cards(card list that x,y are ?,? )

## Trying to read assets from first card in hand
#cc = json_utils.extract_values(card_positions['Rectangles'][2], 'CardCode')
#print("CardCode = ", cc[0][1])



#---------------- Getting game result and checking if game is still in progress --#
#game_result = json.loads(requests.get("http://127.0.0.1:21337/game-result").text)
#print("Game Result : ", game_result)
