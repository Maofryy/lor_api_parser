

class Card:
    # Variables from game client api
    CardCode = 0         # class variable shared by all instances
    TopLeftX = 0
    TopLeftY = 0
    Width = 0
    Height = 0
    LocalPlayer = True

    # Variables from stored asset
    attack = 0
    cost = 0
    health = 0
    name = ""
    keywords = []
    spellSpeed = ""
    type = ""
    description = ""

    # Other created Variables
    CenterX = 0
    CenterY = 0
    active = False
    copies = 0

    def __init__(self, asset, nb):
        self.CardCode = asset['cardCode']
        self.attack = asset['attack']
        self.cost = asset['cost']
        self.health = asset['health']
        self.name = asset['name']
        self.keywords = asset['keywords']
        self.spellSpeed = asset['spellSpeed']
        self.type = asset['type']
        self.description = asset['descriptionRaw']
        self.copies = nb

    def draw(self, obj):
        self.TopLeftX = obj.TopLeftX
        self.TopLeftY = obj.TopLeftY
        self.Width = obj.Width
        self.Height = obj.Height
        self.LocalPlayer = obj.LocalPlayer
        self.CenterX = round((self.TopLeftX + self.Width) / 2)
        self.CenterY = round((self.TopLeftY + self.Height) / 2)
        self.active = True

    def die(self):
        self.TopLeftX = 0
        self.TopLeftY = 0
        self.Width = 0
        self.Height = 0
        self.CenterX = 0    
        self.CenterY = 0
        self.active = False

    def take1(self):
        self.copies -= 1
        if (self.copies <= 0):
            self.die()

    def __str__(self):
        string = str(self.copies)+" : "
        string += self.name+': '
        #Tab according to name length
        tabs = int((32-len(string))/8)+1
        for x in range(tabs):
            string += '\t'
        string += str(self.cost)+"-cost "
        if (self.type == "Unit"):
            string += str(self.attack)+'\\'+str(self.health)+" "
        #else:
            #string += self.spellSpeed+"speed "
        if (self.keywords):
            string += 'Keywords : '
            for k in self.keywords:
                string += k+', '
        return string
    
    def printDescription(self):
        return self.description.replace('\n', '').replace('\r', ' ')