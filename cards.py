from card import Card

class Cards:
    name = ""
    number = 0
    list = []

    def __init__(self, cardlist, name, sets):
        self.name = name
        # push one by one and increment
        for card in cardlist:
            #find set for card
            set = sets[int(card[1])-1]
            #find asset for card
            for c in set:
                if (c['cardCode'] == card):
                    asset = c
            if (asset == None):
                print("Error: no card found")
            self.list.append(Card(asset, cardlist[card]))
            self.number += 1

    def __str__(self):
        string = ""
        for c in self.list:
            string += c.__str__()+'\n'
        return string